[Lien vers le canal teams dédié à gitlab](https://teams.microsoft.com/l/channel/19%3a4abd1ad70d074fbea6439227726f7a03%40thread.skype/GitLab?groupId=71434d04-7d7b-4b94-9213-0c9a7d3a69c2&tenantId=b2597740-753d-430e-926c-984efb259e8d)

Liens vers les 3 salles teams utilisées : 

- [Salle 1, Thibault]()
- [Salle 2, Timothée]()
- [Salle 3, Wassila]()


# Découverte Gitlab

Ce dossier contient le support de cours et les exercices pour une formation découverte au logiciel de suivi de version Git, à l'interface  web Gitlab permettant d'interagir avec ce dernier via un navigateur.

Le but principal de la formation est de présenter les avantages du versionnage de fichiers pour les projets orientés données et les projets applicatifs.  
La partie pratique vise à rendre autonome les participants pour créer un projet git et suivre ses modifications à partir de l'éditeur VSCode.

## Prérequis pour la partie pratique

A effectuer avant la formation si l'on participe à la partie pratique.

- **Création d'un compte gitlab :** [Suivre ce tutoriel](https://gitlab.com/DREES_code/public/tutoriels/-/blob/master/tutos/INSCRIPTION_GITLAB.md) 
- **Si ce n'est pas déjà fait, demande d'ajout au [groupe gitlab HAS](https://gitlab.com/has-sante/) : **Envoyez un mail à data@has-sante.fr**. Cette étape est nécessaire pour avoir accès aux différents projets privés de la HAS.
- **Installation des logiciels nécessaires sur votre poste windows :** [le logiciel de versionnage git](https://gitlab.com/has-sante/wiki-data/-/wikis/Windows/Configuration-ordinateur-data#git) et [l'éditeur de code vscode](https://gitlab.com/has-sante/wiki-data/-/wikis/Windows/Configuration-ordinateur-data#visual-studio-code-vscode) 

Si vous rencontrez des difficultés sur l'un de ces points:
- envoyez-nous un message : via teams sur [le canal dédié](https://teams.microsoft.com/l/channel/19%3a4abd1ad70d074fbea6439227726f7a03%40thread.skype/GitLab?groupId=71434d04-7d7b-4b94-9213-0c9a7d3a69c2&tenantId=b2597740-753d-430e-926c-984efb259e8d) ou si pas de réponse rapide directement à m.doutreligne@has-sante.fr.

## Déroulé 
### Théorie, 1h

Partie collective : sur un canal teams dédié à gitlab, nous planifierons une réunion teams :

- Accueil des participants et introduction : 10 min
- Présentation de la partie théorique : 30 min + 10 min questions
- Présentation des exercices : 10 min


### Pratique, 3h + interaction avec les formateurs

Accompagnement individuel : Chaque participant recevra une invitation sur un canal teams de suivi avec un accompagnateur. Nous publierons sur le canal teams les liens vers les 3 canaux. Une présentation succincte des exercices sera effectuée à 14h puis les participants suivront les instructions sur le projet gitlab de la formation (invitations gitlabs envoyées préalablement avec les mails d’inscription).

Suivez les étapes des exercices présents dans  A tout moment, si vous êtes bloqués
- Problèmes d'installation ou de configuration VSCode + extensions git + git bash 
- Exploration de l'interface gitlab
- Exercice premier commit + clonage
- Exercice collaboration

## Pour aller plus loin

Les formations référencées par la mission data concernant git sont disponibles [ici](https://gitlab.com/has-sante/wiki-data/-/wikis/Tutoriels/formations#git).

Les principales références sont : 
- [La formation Travail collaboratif avec R](https://linogaliana.gitlab.io/collaboratif/git.html) par un agent de l'INSEE détaille les principes de git avec R et Rstudio et donne de bonnes bases pour apprendre à coder en R.
- [Documentation sur l'utilisation de git sur VSCode](https://code.visualstudio.com/docs/editor/versioncontrol)
- [Cours introductif sur datacamp](https://learn.datacamp.com/courses/introduction-to-git) : assez ludique mais en anglais
- [Livre de référence complet sur le fonctionnement de git](https://git-scm.com/book/fr/v2)


### Ressources internes

Des offres mises à disposition par le service formation de la HAS sont [consultables sur le wiki HAS](https://gitlab.com/has-sante/wiki-data/-/wikis/Tutoriels/formations#offres-de-formation)
