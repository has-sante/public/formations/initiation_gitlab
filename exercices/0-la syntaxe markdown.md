# La syntaxe markdown 

Pour toute la durée de cette session pratique, nous utilisons la syntaxe **Markdown**. 

**Késako ? :** Markdown est un système de publication et de formattage de texte minimaliste créé dans le but d'offrir une syntaxe facile à lire et à écrire. Elle est couramment utilisée notamment pour documenter du code ou des jeux de données. Les fichiers sont stockés au format texte classique et sont directement interpétrable par des navigateurs web grâce à des balises. Cela en fait un format d'édition et de publication de texte idéal pour des contenus internet.

- Ci-dessous [la feuille de rappels Markdown avec tout ce qu'il faut savoir sur les balises](https://documentation-snds.health-data-hub.fr/files/images/tutoriel_gitlab/2020-03-30_HDH_Cheatsheet-markdown_MLP-2.0.pdf) (éditée par le Health Data Hub) :

![markdown_cheatsheet](/imgs/HDH_Cheatsheet-markdown.png)

## Aller plus loin

- L'essentiel sur la syntaxe markdown [sur le site de formation openclassrooms](https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown)

- [Un post de blog détaillé pour les curieux](https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/)

- [L'article wikipédia est très synthétique et rappelle les balises de base](https://fr.wikipedia.org/wiki/Markdown)