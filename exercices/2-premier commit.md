# Création d'un projet, premier et second commits, consultation de l'historique

## Exercice 1 : création de projet en local

Cet exercice va vous apprendre à créer votre premier projet git et à faire votre premier commit.

- Créer un nouveau projet en créant un dossier windows :

![creation_dossier](/imgs/creation_dossier.PNG)

- Ouvrir VSCode puis sélectionner ce nouveau dossier : 

![ouvrir_dossier](/imgs/ouvrir_dossier.PNG)

- Aller dans l'onglet git (source control) et initier un dépôt git dans ce nouveau projet. 

![git_init](/imgs/git_init.PNG)

**Note :** Cette étape créée dans votre projet le sous-dossier `.git` avec tout le nécessaire pour effectuer le contrôle de version. Par défaut, ce dossier est invisible, mais vous pouvez y accéder en affichant les éléments masqués dans l'explorateur de fichier windows (un équivalent en ligne de commande est ls -a).

- Créer un premier fichier `README.md` et le sauvegarder : 

![first_readme](/imgs/first_readme.png)

- Faire son premier commit :

![first_commit](/imgs/first_commit.PNG)


## Exercice 2 : Ajouter son projet à un serveur distant (remote)

En général, on veut s'assurer que son projet n'existe pas que sur son ordinateur. Le pousser sur un serveur distant permet de s'assurer qu'il existe une sauvegarde. Cela permet aussi à d'autres gens de venir le consulter s'ils y sont autorisés ou bien si le projet est public. GitLab ou Github sont les deux serveurs distants ouverts sur internet les plus connus. 

- Créer le projet en distant et ajouter un remote :
    - Aller sur gitlab.com et créer le projet en remote : 
    ![gitlab_create](/imgs/gitlab_create.PNG)    
    - Créer *depuis blanc* : 
    ![create_blank](/imgs/create_blank.PNG)    
    - Finir la création du projet : 
    ![gitlab_create_final_step](/imgs/gitlab_create_final_step.PNG)

- Ajouter le remote depuis son poste local : 
    - Renseigner le nom du remote : 
    ![add_remote_1](/imgs/add_remote_1.PNG)
    - Copier l'url depuis l'interface gitlab : 
    ![add_remote_url](/imgs/add_remote_url.PNG)
    - Et la renseigner dans VSCode : 
    ![add_remote_2](/imgs/add_remote_2.PNG)

- Il ne reste plus qu'à pousser pour la première fois : 

![push_master](/imgs/push_master.PNG)


- En actualisant la page web du projet (sur gitlab), on voit bien que le fichier a été poussé (téléchargé sur le dépôt distant) et apparaît comme dans notre éditeur VSCode.


## Exercice 3 : Second commit et différence entre deux versions d'un fichier.

Maintenant, nous allons modifier notre premier fichier et regarder comment fonctionne la comparaison de version appelée **diff**.

- Ouvrir dans VSCode (en local donc), le fichier `README.md` que vous avez créé, puis ajouter une ligne : 

![readme_change](/imgs/readme_change.PNG)

- Ouvrir l'onglet git. Le fichier `README.md` apparaît dans change. En double-cliquant dessus, on voit apparaître les deux versions du fichier, la version originale et celle après notre changement. 

![readme_diff](/imgs/readme_diff.PNG)

- Commiter (enregistrer) la modification. Noter qu'il faut toujours renseigner un message de commit : 

![second_commit](/imgs/second_commit.PNG)

- Pousser la modification sur le dépôt distant. De cette façon, des collaborateurs éventuels sont au courant des changements effectués :

![second_push](/imgs/second_push.PNG)

- On peut toujours comparer la version actuelle d'un fichier avec une version précédente en regardant son historique de commits : 
  - Ouvrir l'historique du fichier :

![open_file_history](/imgs/open_file_history.PNG)

  - Comparer avec la version actuelle : 

![hist_diff](/imgs/hist_diff.PNG)

# Exercice 4 : Clonage d'un projet pré-existant

Souvent, on désire récupérer un projet préexistant déjà dans un dépôt distant et travailler dessus à partir de son poste de travail personnel. Un exemple est celui d'une collaboration sur un projet initiés par d'autres personnes.

Par exemple, pour récupérer ce projet de formation, il suffit de :

- Récupérer l'url de clonage du projet en allant sur la page web correspondante : 
![git_clone](/imgs/git_clone.PNG)

- Renseigner le projet dans une nouvelle fenêtre VSCode :
![vscode_clone](/imgs/vscode_clone.PNG)

- Une fois l'emplacement selectionné, le projet est téléchargé (plus ou moins long selon la taille totale du projet) puis on vous propose d'ouvrir le projet dans VSCode. Vous pouvez alors travailler dessus avec vos propres changements, commit et push.

## Conclusion

Vous avez désormais toutes les clés en main pour créer un nouveau projet git, le synchroniser avec un dépôt distant et suivre les changements à propos d'un fichier particulier.

Dans les parties suivantes, nous verrons comment collaborer à plusieurs sur un même projet.

## Pour aller plus loin

### Faire la même chose, mais plus rapidement avec la ligne de commande

Si l'on est à l'aise avec la ligne de commandes, on peut faire les différentes étapes de création du projet plus rapidement. 

- Aller sur gitlab et créer un projet comme détaillé plus haut.

- Ouvrir powershell, se déplacer à l'endroit où l'on veut créer le projet, par exemple dans un dossier pré-existant `C:\Users\m.doutreligne\mon_dossier_projets`:

```
cd ~/mon_dossier_projets
```

- Cloner le nouveau projet distant encore vide, créer un fichier README et l'envoyer à la version distante : 

```
git clone git@gitlab.com:strayMat/mon_premier_git.git
cd mon_premier_git
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```



# Problèmes rencontrés
- git host key verification failed vscode : `ssh -o StrictHostKeyChecking=no git@gitlab.com` in powershell
- invalid git load key ....