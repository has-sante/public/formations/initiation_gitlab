# Collaboration

Vous allez apprendre dans cet exercice à collaborer sur un même projet par petits groupes. Nous aborderons les notions de branche, merge request, issues. 

## Cas d'usage

 Vous travaillez sur un projet comportant du code **avec plusieurs autres personnes**. Le chef de projet repère une amélioration à mener (dans la collectes des données, le modèle statistique utilisé, ou bien la restitution des résultats par exemple). Cette personne va décrire l'amélioration à mettre en place, échanger avec les développeurs pour mieux définir l'objectif à atteindre. Puis ceux-ci vont mettre en oeuvre (implémenter) la modification tout en conservant le projet principal intact. Il faut ensuite vérifier que cette modification correspond bien aux attentes avant de l'intégrer au projet principal.

 Gitlab permet de suivre et faciliter toutes les étapes de ce cas d'usage en coordonnant les différents acteurs du projet.

 Ce cas d'usage est une **méthode d'organisation**, appelée git workflow qui s'appuie sur **une fonctionnalité de git**, les branches. Ces principes restent appropriés pour des projets ne contenant pas de code mais incluant de nombreux acteurs (ex. [les demandes de fusion pour la documentation collaborative du SNDS](https://gitlab.com/healthdatahub/documentation-snds/-/merge_requests)).

Le schéma du processus complet est le suivant. Pour une excellente explication étape par étape du processus de travail par branche, allez voir [sur le site de la documentation du SNDS](https://documentation-snds.health-data-hub.fr/contribuer/guide_contribution/introduction_gitlab.html#naviguer-dans-gitlab) : 

![github flow](/imgs/github_flow.jpg)

## Etape par étape

Nous allons procéder en plusieurs étapes. Par petits groupes (2 ou 3 personnes), désigner un chef de projet et des développeurs. Chacun aura des rôles différents dans le tutoriel


## Partie pratique

### Récupérer le projet exemple existant

Le/la chef/fe de projet créé une divergance (fork) du template de [projet de collaboration](https://gitlab.com/has-sante/public/formations/template_collaboration) : 

![collab_fork](/imgs/collab_fork.PNG)

Cette manipulation lui permet d'avoir une copie sans modifier le template qui servira pour travailler à plusieurs. 

Il/elle envoie le lien aux autres participants (celui-ci doit contenir son nom d'utilisateur gitlab): 

![collab_send_link](/imgs/collab_send_link.PNG)

Ces derniers clonent le projet comme vu dans l'exercice 1. 

### Créer une issue et la discuter

Le/La Chef/fe de projet crée un ticket (issue) sur l'interface web du projet. Il demande par exemple que chacun ajoute dans le fichier .README son nom et une courte description personnelle. 

![collab_new_issue](/imgs/collab_new_issue.PNG)

![collab_description_issue](/imgs/collab_description_issue.PNG)

Les autres participants en cliquant sur l'onglet issues peuvent voir l'issue et la discuter : 
![collab_discute_issue](/imgs/collab_discute_issue.PNG)

### Créer une branche

Une fois que tout le monde est d'accord sur le processus à suivre. Les développeurs peuvent faire les changements.

Pour cela, ils vont créer une nouvelle branche sur laquelle faire le changement. On va d'abord checkout, puis créer une nouvelle branche à laquelle on donne un nom explicite (ex. ajout_description).

![collab_checkout](/imgs/collab_checkout.PNG)

![collab_create_branch](/imgs/collab_create_branch.PNG)

### Faire des modifications sur la branche

Le développeur fait les modifications demandées. Cela peut-être comme ici quelques lignes dans un seul fichier. Ou bien l'ajout d'une nouvelle fonctionnalité qui nécessite de changer plusieurs fichiers.

Ne pas oublier de sauvegader les changements, puis de commiter.

![collab_changements](/imgs/collab_changements.PNG)

![collab_commit](/imgs/collab_commit.PNG)

Ensuite, il faut pousser la nouvelle branche sur le dépôt central.

![collab_push](/imgs/collab_push.PNG)

![collab_push_upstream](/imgs/collab_push_upstream.PNG)

NB: Pendant ce temps, les autres personnes peuvent continuer à travailler sur la branche principale ou créer d'autres branches concernant d'autres changements.

### Emettre une demande de fusion (Merge Request)

Le développeur peut ensuite soumettre une demande de fusion afin que sa modification soit intégré au projet principal.

Pour cela, il faut retourner sur l'interface web et aller dans l'onglet fusion.

![collab_pr](/imgs/collab_pr.PNG)

**Attention** bien mettre comme branche cible, le projet du chef de projet ! Sinon vous allez faire la demande de fusion sur le groupe HAS et n'aurez pas le droit d'accepter la fusion.

![collab_pr_target](/imgs/collab_pr_target.PNG)

On décrit ce qu'on a voulu faire dans cette nouvelle branche, l'état d'avancement (est-ce prêt à intégrer...).

![collab_description_issue](/imgs/collab_description_issue.PNG)


### Revoir la merge request, la discuter

Avant d'intégrer les changements, il est souvent intéressant de revoir la demande de fusion (merge request) afin de s'assurer qu'elle ne casse pas du code de la branche principale, que les modifications apportées sont justes, bref que tout va bien ! 

Une discussion est engagée sur la page de la demande de fusion et cela peut donner lieu à de nouveaux changements sur la branche de travail. Plus la fonctionnalité ajoutée est complexe, plus il y a d'allers retours à cette étape. 

![collab_pr_review](/imgs/collab_pr_review.PNG)

NB : On peut voir les changements par rapport à la branche principale en allant dans l'onglet changements. L'ancienne version est à gauche avec les parties modifiées en rouge, la nouvelle version à droite avec les modifications en vert. 

![collab_pr_changes](/imgs/collab_pr_changes.PNG)


### Fusionner la nouvelle branche

Enfin, lorsque tout le monde est ok, on fusionne la branche dans le branche principale afin d'intégrer les nouveaux changements.

![collab_pr_accept](/imgs/collab_pr_accept.PNG)


Désormais la branche principale contient bien les nouveaux changements.

NB: C'est en générale une bonne pratique de supprimer la branche de travail pour garder un projet principal propre sans une multitude de branches portant sur des fonctionnalités différentes.

# Aller plus loin

- [Git 101](https://towardsdatascience.com/git-workflow-for-data-scientists-c75445f23f44) et [GitHub for Data Scientists](https://towardsdatascience.com/collaborate-on-github-like-pro-part2-26a6790e8f93) : inspirations pour ce tutoriel

- [Le github flow](https://www.nicoespeon.com/fr/2013/08/quel-git-workflow-pour-mon-projet/#le-github-flow) : Une organisation des branches facile avec seulement deux types de branches (master et features) pour des projets sans gros enjeu de production en temps réél. [Article original](http://scottchacon.com/2011/08/31/github-flow.html)


- [Le gitflow](https://www.nicoespeon.com/fr/2013/08/quel-git-workflow-pour-mon-projet/) : Une organisation plus complexe des branches orientée vers les gros projets en production nécessitant des releases.

Ces deux exemples sont des extrêmes et l'organisation d'un projet navigue souvent entre les deux selon les besoins spécifiques.

