# Exercice 1 - Explorer l'interface gitlab


L'interface gitlab est très riche et peut sembler perturbante au premier abord. Prenez le temps de l'explorer un peu : 

- Aller trouver le projet Adex en parcourant l'arborescence à partir [du groupe HAS](https://gitlab.com/has-sante). 

- Aller voir le projet d'analyse des retours patients, e-satis en utilisant la barre de recherche (taper esatis). 

- Aller voir le wiki de l'équipe data : https://gitlab.com/has-sante/wiki-data/-/wikis/home

- Trouver le groupe du health data hub sur gitlab.

- Aller voir l'activité du groupe HAS : https://gitlab.com/groups/has-sante/-/activity 

- Explorer gitlab en tapant le nom d'un projet qui nous intéresse, par exemple *chercher opendamir dans tout gitlab*